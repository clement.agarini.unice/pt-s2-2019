# Projets tutorés IUT Informatique - S2 2020

Le but de ce projet est de concevoir et développer un jeu d'échecs en ligne de commande.

## Organisation

Vous devez former 2 groupes de projet tuteuré au sein de chaque groupe de TD.

Vous devez créer un projet sur le serveur [gitlab](https://git-iutinfo.unice.fr) de l'IUT, et nous inviter [nathalie](https://git-iutinfo.unice.fr/feneon) et moi-même [clément](https://git-iutinfo.unice.fr/cagarini).

**Le chef de groupe** doit remplir le google form suivant [https://forms.gle/v2QqTQTAcndqKyaS8](https://forms.gle/v2QqTQTAcndqKyaS8) pour nous indiquer:

- Le nom du groupe
- Le lien de votre projet sur gitlab
- La liste des membres du groupes

Vous pouvez nous contacter sur le [slack du département info 2019-2020](https://depinfoiutnic-voq7258.slack.com) dans le channel **pt-s2t**.

Merci d'envoyer tous vos messages dans ce channel, les réponses que nous vous apporterons pourront servir à vos camarades.

## Le jeu

Vous devez concevoir et implémenter un jeu d'échec avec les fonctionnalités **minimales** suivantes :

- Je dois être capable de jouer tour par tour, joueur (humain) contre autre joueur (humain)
- Pour jouer, je veux utiliser une interface en ligne de commande
  - Vous devez afficher dans la console un plateau d'échec
  - Pour bouger une pièce le joueur doit saisir la case d'origine et la case de destination
- Toutes les pièces doivent pouvoir se déplacer conformément aux règles
- Je dois être capable de manger les autres pièces
- Je dois être capable de mettre l'adversaire en position d'échec et en position d'échec et mat
- À la fin d'une partie je dois pouvoir recommencer une nouvelle partie

Réfléchissez à tous les cas possibles :

- Si le mouvement est invalide, le joueur doit rejouer
- Si le joueur est en échec et ne fait pas un mouvement lui permettant de sortir de la situation d'échec, il doit rejouer
- Si le joueur est échec et mat la partie est finie, les joueurs doivent pouvoir en recommencer une nouvelle
- etc.

### Affichage

Vous **devez** affichez un jeu d'échec dans le format standard:

- Les lettres de A à H en bas
- Les chiffres sur la gauche du 8 au 1

Exemple:

![Sample chess game](sample_game.png "Sample chess game")

### Chargement de fichiers

Nous devons pouvoir charger le jeu dans un certain état, vous devez me permettre de charger des fichiers avec le format suivant:

```csv
Tn,Cn,Fn,ReN,RoN,Fn,Cn,Tn
Pn,Pn,Pn,Pn,Pn,Pn,Pn,Pn
V,V,V,V,V,V,V,V
V,V,V,V,V,V,V,V
V,V,V,V,V,V,V,V
V,V,V,V,V,V,V,V
Pb,Pb,Pb,Pb,Pb,Pb,Pb,Pb
Tb,Cb,Fb,ReB,RoB,Fb,Cb,Tb
```

Les pièces noires:

- Tn: Tour noire
- Cn: Cavalier noir
- Fn: Fou noir
- ReN: Reine noire
- RoN: Roi noir
- Pn: Pion noir

Les pièces blanches:

- Tb: Tour blanche
- Cb: Cavalier blanc
- Fb: Fou blanc
- ReB: Reine blanche
- RoB: Roi blanc
- Pb: Pion blanc

Les pièces sont disposés comme sur le jeu d'échec d'exemple ci-dessus, sur la gauche de **8** (en haut) à **1** (en bas), sur le bas de **a** (à gauche) à **h** (à droite)

Le fichier contiendra **toujours** 8 lignes avec chacune 8 "cellules" séparés par des virgules.

**/!\ Les cases vides contiennent un V**

Le fichier est au format `CSV`.

Quand je démarre le jeu, je veux pouvoir choisir de

1. Démarrer une nouvelle partie
2. Charger un fichier de jeu (.csv)

Example de fichier que je veux pouvoir charger:

Coup du berger

```csv
Tn,V,Fn,ReN,RoN,Fn,V,Tn
Pn,Pn,Pn,Pn,V,Pn,Pn,Pn
V,V,Cn,V,V,Cn,V,V
V,V,V,V,Pn,V,V,ReB
V,V,Fb,V,Pb,V,V,V
V,V,V,V,V,V,V,V
Pb,Pb,Pb,Pb,V,Pb,Pb,Pb
Tb,Cb,Fb,V,RoB,V,Cb,Tb
```

Correspond à :
![Coup du berger](coup_du_berger.png "Coup du berger")

Voici un lien pour récupèrer le fichier: [coup_du_berger.csv](coup_du_berger.csv).

### Saisi des mouvement

La saisi des mouvements doit s'effectuer en entrant les coordonnées dans le format suivant:

b1 c3

b1: Case d'origine (cavalier blanc en début de part)

c3: Case de destination

### Projet de base

Je vous ai préparé un projet maven que vous **devez** utiliser, vous pouvez le récupérer ici:
[https://gitlab.com/clement.agarini.unice/chess-minimal-project](https://gitlab.com/clement.agarini.unice/chess-minimal-project)

Lors du rendu, vous devez faire attention à ce que:

- Je puisse éxecuter les tests et le coverage avec la commande de test indiqué dans le [README.md](https://git-iutinfo.unice.fr/cagarini/chess-minimal-project) du projet d'exemple
- Je puisse démarrer le jeu avec la commande indiqué dans le [README.md](https://git-iutinfo.unice.fr/cagarini/chess-minimal-project) du projet d'exemple

### Bonus

En bonus, pour avoir des points supplémentaires (voir barème ci-dessous) vous pouvez :

1. Implémenter **l'ensemble** des règles supplémentaires suivantes
   - Roque
   - Prise en passant
   - Promotion pion
   - Échec et pat
2. Implémenter une "Intelligence artificielle" (Un bot contre qui on peut jouer)
3. Implémenter une interface graphique

### Conception

**Date de rendu: 19/03/2020 à 23h59**

**Notation sur 20**

Vous devez rendre un diagramme des classes (UML) complet qui contient la hiérarchie des classes avec toutes les méthodes que vous implémenterez par la suite. Pensez à être précis sur la visibilité des méthodes et sur les cardinalités.

Pour ce rendu, vous êtes libres du support, vous pouvez le faire sur papier et le numériser ou bien le saisir sur un [outil numérique](https://www.draw.io/).

### Développement

**Date de rendu: 03/06/2020 à 23h59**

**Notation sur 20**

Vous devez rendre un "repository" git à jour avec une application écrite en java fonctionelle, si votre application ne compile pas vous aurez **0**.

Vous devez faire un zip de l'état de votre repository (ne pas oublier d'inclure le répertoire **.git** dans le zip) et faire un upload sur moodle à la date du 04/06/2020.

Vous avez le droit de copier du code source, ou de vous inspirer de ce que vous pouvez trouver sur internet, il est par contre interdit de rendre un programme complet trouvé sur internet.

Dans le cas ou vous copiez du code source ou si vous vous inspirez de solutions trouvées sur internet vous devez :

- Citez la source dans un commentaire ou dans la javadoc avec un lien
- Compiler l'ensemble des liens dans le README.md à la racine du projet

Pour avoir 20/20 vous devez :

- Avoir un README.md à la racine du projet qui explique comment compiler le code et jouer à votre jeu et qui contient l'ensemble des liens vers les sources dont vous vous êtes inspirés
- Avoir un code aligné avec votre conception **5 points**
  - Si vous faites des modifications sur la conception en cours de développement, vous devez refaire un rendu du diagramme à la fin du projet
- Avoir implémenté "minimales" l'ensemble des fonctionnalités décrites [ici](#le-jeu) **10 points**
- Avoir écrit un ensemble de tests avec JUnit qui couvre au moins 70% du code **5 points**
  - Vous devez vérifier le coverage avec le plugin maven comme expliqué ci-dessus

Vous serez noté en partie pour votre **contribution individuelle** au projet, pensez à commiter en votre nom, nous nous servirons de l'historique des commit git pour savoir ce que vous avez fait. Nous nous servirons aussi de ce que vous remplirez en suivi de projet.

Vous pouvez obtenir 5 points bonus en implémentant le 1. **ou** le 2. **ou** le 3. décrit [ici](#bonus).

### Date de rendu

| Date de rendu      | Points |
| ------------------ | ------ |
| 1 jour d'avance    | + 1    |
| 1 jour de retard   | - 1    |
| 2 jour de retard   | - 2    |
| 3 jour de retard   | - 3    |
| 4 jour de retard   | - 4    |
| 5 jour de retard   | - 5    |
| > 5 jour de retard | 0/20   |

Si on prend les dates de rendu précédentes: 19/03/2020) à 23h59, il faut rendre le 18/03 pour avoir 1 point bonus.
Même chose pour le rendu du: 03/06/2020 à 23h59, il faut rendre le 02/06 pour avoir un point bonus.
